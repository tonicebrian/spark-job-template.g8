import sbt._

object Version {
  val spark     = "$sparkVersion$"
  val scalatest = "3.0.1"
}

object Dependencies {

  import Version._

  val projectDependencies = Seq (
    "org.apache.spark"   %% "spark-core"      % Version.spark % "provided"
    , "org.apache.spark" %% "spark-streaming" % Version.spark % "provided"
    
    , "org.scalatest"    %% "scalatest"       % Version.scalatest % "test"
  )
}
