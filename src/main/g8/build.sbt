name := "$name$"

version := "1.0"

scalaVersion := "2.11.8"

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xfatal-warnings", "-language:implicitConversions")

unmanagedSourceDirectories in Compile += sourceDirectory.value / "generated" / "scala"

(testOptions in IntegrationTest) += Tests.Argument(TestFrameworks.ScalaTest, "-u", "target/junit-report", "-o")

(testOptions in Test) += Tests.Argument(TestFrameworks.ScalaTest, "-u", "target/junit-report", "-o")

libraryDependencies ++= Dependencies.projectDependencies

parallelExecution in Test := false

artifact in (Compile, assembly) ~= { art =>
   art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)
