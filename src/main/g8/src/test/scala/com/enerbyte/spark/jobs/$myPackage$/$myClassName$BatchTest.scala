package com.enerbyte.spark.jobs.$myPackage$

import com.enerbyte.spark.utils.SparkSpec
import org.scalatest.{MustMatchers, GivenWhenThen, FlatSpec}

/**
 * Created by cebrian on 10/09/15.
 */
class $myClassName$BatchTest extends FlatSpec
with SparkSpec
with MustMatchers
with GivenWhenThen
{
  "The batch job" should "reverse input lines" in {
    Given("A set of lines")
    val lines = Seq("To be or not to be.", "That is the question.")
    val rdd = sc.parallelize(lines)

    When("We run the spark transformation")
    val result = $myClassName$BatchJob.run(rdd).collect()

    Then("We have those lines reversed")
    val expected = lines.map(_.reverse)

    result must equal (expected)
  }

}
