package com.enerbyte.spark.utils

import java.nio.file.Files

import org.apache.spark.streaming.{Duration, ClockWrapper, Seconds, StreamingContext}
import org.scalatest.Suite

/**
 * Class based on code from https://github.com/mkuthan/example-spark
 */
trait SparkStreamingSpec extends SparkSpec {
  this: Suite =>

  private var _ssc: StreamingContext = _

  def ssc = _ssc

  private var _clock: ClockWrapper = _

  def clock = _clock

  implicit val sliceDuration : Duration

  val checkpointDir = Files.createTempDirectory(this.getClass.getSimpleName)

  conf.set("spark.streaming.clock", "org.apache.spark.streaming.util.ManualClock")

  override def beforeAll(): Unit = {
    super.beforeAll()

    _ssc = new StreamingContext(sc, sliceDuration)
    // With Spark 1.5.0 this doesn't work for tests.
    //_ssc.checkpoint(checkpointDir.toString)

    _clock = new ClockWrapper(ssc)
  }

  override def afterAll(): Unit = {
    if (_ssc != null) {
      // TODO: check why context can't be stopped with stopGracefully = true
      _ssc.stop(stopSparkContext = false, stopGracefully = false)
      _ssc = null
    }

    super.afterAll()
  }
}