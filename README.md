# Template for creating Spark Jobs

This is a template for creating Spark Jobs either for batch processing or for
Spark Streaming. The template provides:

- Placeholder for Batch and Streaming Spark jobs
- Unit testing setup for testing those jobs

The required variables are:

- *sparkVersion*
- *packageName* that will be appended to the route *com.enerbyte.spark.jobs*
- *className* that denotes the object holding the job

In order to create a base project install
[giter8](https://github.com/n8han/giter8) and then run

```
$ g8 https://bitbucket.org/enerbyte/spark-job-template.g8
```

